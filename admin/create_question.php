<?php

require_once(__DIR__ . "/bootstrap.php");
require_once(__DIR__ . "/../models/quiz.php");

global $page;
$page = [
    "id"   => "createquestion_page"
    ,"name" => "Create Question"
    ,"tpl"  => "pages/create_question.phtml"
    ,"data" => []
];

function insertquestionAction () {
    global $page;
    global $dbConn;

    $quizid = $_REQUEST['inputquizid'];
    $questionname = $_REQUEST['inputquestionname'];
    $questiontext = $_REQUEST['inputquestiontext'];
    $questionanswer = $_REQUEST['inputquestionanswer'];
    $questionpoints = $_REQUEST['inputquestionpoints'];
    $insertquestion = insertQuestion( $dbConn, $quizid, $questionname, $questiontext, $questionanswer, $questionpoints);
    if (!empty($insertquestion)) {
        header("Location: http://localhost/SimpleQuizBuilder/admin/home.php");
    } else {
        $page["data"]["errors"] =[];
        $page["data"]["errors"][] ="Invalid quiz informations!";
    }

}

if (!isset($_SESSION["action"])) {
    if (!empty($_REQUEST["action"])) {
        ($_REQUEST["action"]."Action")();
        header("Location: http://localhost/SimpleQuizBuilder/admin/edit_question.php");
    }
}

echo renderTpl( "html_page.phtml", ["page"=>$page]);