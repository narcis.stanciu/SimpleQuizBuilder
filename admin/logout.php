<?php
require_once(__DIR__ . "/bootstrap.php");

session_destroy();

header("Location: http://localhost/SimpleQuizBuilder/admin/login");
/**
 * The code above it's closing the current session and redirects the user to the login page
 */