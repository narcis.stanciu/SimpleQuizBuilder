<?php

require_once(__DIR__ . "/bootstrap.php");
require_once(__DIR__ . "/../models/quiz.php");

global $page;
$page = [
    "id"   => "createquiz_page"
    ,"name" => "Create Quiz"
    ,"tpl"  => "pages/create_quiz.phtml"
    ,"data" => []
];

function insertquizAction () {
    global $page;
    global $dbConn;

    $quizname=$_REQUEST['inputquizname'];
    $insertquiz = insertQuiz( $dbConn, $quizname);
    if (!empty($insertquiz)) {
        header("Location: http://localhost/SimpleQuizBuilder/admin/home.php");
    } else {
        $page["data"]["errors"] =[];
        $page["data"]["errors"][] ="Invalid quiz informations!";
    }

}

if (!isset($_SESSION["action"])) {
    if (!empty($_REQUEST["action"])) {
        ($_REQUEST["action"]."Action")();
        header("Location: http://localhost/SimpleQuizBuilder/admin/home.php");
    }
}

echo renderTpl( "html_page.phtml", ["page"=>$page]);