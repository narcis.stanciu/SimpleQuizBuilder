<?php
require_once(__DIR__ . "/bootstrap.php");

if (!isset($_SESSION["user"])) {
    header("Location: http://localhost/SimpleQuizBuilder/admin/login.php");
    exit();
}
print_r($_SESSION["user"]);
/**
 * If the Session's user is not set,
 * then it redirects to the login page
 */