<?php
require_once(__DIR__ . "/bootstrap.php");
require_once(__DIR__ . "/../models/user.php");

global $page;
$page = [
    "id"   => "register_page"
    ,"name" => "Register Quiz"
    ,"tpl"  => "pages/register.phtml"
    ,"data" => []
];

function registerAction () {
    global $page;
    global $dbConn;

    $firstname=$_POST['inputFName'];
    $lastname =$_POST['inputLName'];
    $number   =$_POST['inputNumber'];
    $email    =$_POST['inputEmailr'];
    $password =$_POST['inputPasswordr'];

    $registeruser = registerUser( $dbConn, $email, $number, $password, $firstname, $lastname);

    if (!empty($registeruser)) {
        header("Location: http://localhost/SimpleQuizBuilder/admin/login.php");
    } else {
        $page["data"]["errors"] =[];
        $page["data"]["errors"][] ="Invalid user informations!";
    }

}

if (!isset($_SESSION["registeruser"])) {
    if (!empty($_REQUEST["action"])) {
        ($_REQUEST["action"] . "Action") ();
    }
}

echo renderTpl( "html_page.phtml", ["page"=>$page]);