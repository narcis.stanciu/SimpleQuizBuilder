<?php

require_once(__DIR__ . "/bootstrap.php");
require_once(__DIR__ . "/../models/user.php");
require_once(__DIR__ . "/../models/quiz.php");

global $page;
$page = [
    "id"   => "home_page"
    ,"name" => "Home Quiz"
    ,"tpl"  => "pages/home.phtml"
    ,"data" => []
];

function getquizesAction () {
    global $dbConn;
    global $page;
    $quizes = getQuizes($dbConn);

    if (!empty($quizes)) {
        $page['data']['quizes'] = $quizes;
    }else{
        echo "ERROR: No Quizes Data!";
    }
}

if (!empty($_REQUEST['action'])) {
    if (function_exists($_REQUEST['action'] . "Action")) {
        ($_REQUEST['action']."Action")();
    }else {
        echo "Function does not exist!";
    }
}else{
    getquizesAction();
}

echo renderTpl( "html_page.phtml", ["page"=>$page]);