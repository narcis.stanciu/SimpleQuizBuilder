<?php
require_once(__DIR__ . "/bootstrap.php");
require_once(__DIR__ . "/../models/user.php");


/**
 * @global structure containing data to be displayed in template
 */
global $page;
$page = [
    "id"   => "login_page"
    ,"name" => "Login Quiz"
    ,"tpl"  => "pages/login.phtml"
    ,"data" => []
];

/**
 * Function loginAction checks the 2 input fields from the login form and if the username is not empty and the password is the correct one,
 * then it sets the Session's user to the one from the form and redirects the user to the main website page,
 * if not then it displays the corresponding errors
 */
function loginAction(){
    global $page;
    global $dbConn;

    $username = $_POST['inputEmail'];
    $password = $_POST['inputPassword'];

    $user = getUserByUserName($dbConn, $username);
    if( !empty($user) && $user[0]["password"] == $password  ){
        unset($user[0]["password"]);
        $_SESSION["user"] = $user;
        header("Location: http://localhost/SimpleQuizBuilder/admin/home");
        exit();
    } else {
        $page["data"]["errors"] =[];
        $page["data"]["errors"][] ="Invalid password!";
    }
}

/**
 * If the user is not logged in (is set) and the login form action isn't empty then,
 * it calls the loginAction function,
 * otherwise if the user is logged in it automatically redirects to the main page
 */
if (!isset($_SESSION["user"])) {
    if (!empty($_REQUEST["action"])) {
        ($_REQUEST["action"] . "Action")();
    }
} else {
    header("Location: http://localhost/SimpleQuizBuilder/admin/home");
    exit();
}

echo renderTpl( "html_page.phtml", ["page"=>$page]);