<?php

require_once(__DIR__ . "/bootstrap.php");
require_once dirname(__DIR__)."/models/quiz.php";

global $page;
$page = [
    "id"   => "editquestion_page"
    ,"name" => "Edit Question"
    ,"tpl"  => "pages/edit_question.phtml"
    ,"data" => []
];

function geteditquestionAction () {
    global $dbConn;
    global $page;
    $geteditquestion = getEditQuestions($dbConn);

    if (!empty($geteditquestion)){
        $page['data']['question'] = $geteditquestion;
    }else{
        echo "ERROR: No Question data!";
    }
}

if (!empty($_REQUEST['action'])){
    if (function_exists($_REQUEST['action']."Action")){
        ($_REQUEST['action'] . "Action")();
    }else{
        echo "Function does not exist!";
    }
}else{
    geteditquestionAction();
}

echo renderTpl( "html_page.phtml", ["page"=>$page]);