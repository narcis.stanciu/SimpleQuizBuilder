<?php

require_once(__DIR__ . "/bootstrap.php");
require_once(__DIR__ . "/../models/quiz.php");

global $page;
$page = [
    "id"   => "editquiz_page"
    ,"name" => "Edit Quiz"
    ,"tpl"  => "pages/edit_quiz.phtml"
    ,"data" => []
];

function editquizAction () {
    global $page;
    global $dbConn;

    $quizname=$_REQUEST['inputquizchange'];
    $id=$_GET['id'];
    $editquiz = editQuiz( $dbConn, $quizname, $id);

    if (!empty($editquiz)) {
        header("Location: http://localhost/SimpleQuizBuilder/admin/edit_quiz.php");
    } else {
        $page["data"]["errors"] =[];
        $page["data"]["errors"][] ="Invalid quiz informations!";
    }

}

function geteditquizAction () {
    global $dbConn;
    global $page;
    $id = $_REQUEST['id'];
    $quiz = getEditQuiz($dbConn, $id);
    if (!empty($quiz)){
        $page['data']['quiz'] = $quiz;
    }else{
        echo "ERROR: No Quiz data!";
    }
}

if (!empty($_REQUEST['action'])){
    if (function_exists($_REQUEST['action']."Action")){
        ($_REQUEST['action'] . "Action")();
    }else{
        echo "Function does not exist!";
    }
}else{
    geteditquizAction();
}

if (!empty($_REQUEST["action"])) {
    ($_REQUEST["action"] . "Action") ();
    header("Location: http://localhost/SimpleQuizBuilder/admin/home.php");
}else{
}

echo renderTpl( "html_page.phtml", ["page"=>$page]);