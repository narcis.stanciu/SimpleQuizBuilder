<?php

require_once(__DIR__ . "/bootstrap.php");
require_once(__DIR__ . "/../models/user.php");

global $page;
$page = [
    "id"   => "modal_page"
    ,"name" => "Home Quiz"
    ,"tpl"  => "pages/modal.phtml"
    ,"data" => []
];


echo renderTpl( "html_page_modal.phtml", ["page"=>$page]);