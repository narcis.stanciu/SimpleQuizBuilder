<?php

require_once(__DIR__ . "/bootstrap.php");
require_once(__DIR__ . "/../models/quiz.php");


global $page;
$page = [
    "id"   => "edit_question_dbo_page"
    ,"name" => "Edit Question DB"
    ,"tpl"  => "pages/edit_question_dbo.phtml"
    ,"data" => []
];

function editquestionAction () {
    global $page;
    global $dbConn;

    $name   =$_REQUEST['inputquestionname'];
    $text   =$_REQUEST['inputtext'];
    $answer =$_REQUEST['inputanswer'];
    $points =$_REQUEST['inputpoints'];
    $id     =$_REQUEST['id'];
    $editquestion = editQuestion( $dbConn, $name, $text, $answer, $points, $id);

    if (!empty($editquestion)) {
        header("Location: http://localhost/SimpleQuizBuilder/admin/edit_question.php");
    } else {
        $page["data"]["errors"] =[];
        $page["data"]["errors"][] ="Invalid question informations!";
    }

}

function getupdatequestionAction (){
    global $dbConn;
    global $page;
    $id = $_REQUEST['id'];
    $updatequestion = getUpdateQuestion($dbConn, $id);

    if (!empty($updatequestion)) {
        $page['data']['updquestion'] = $updatequestion;
    }else{
        echo "ERROR: No data to update!";
    }
}

if (!empty($_REQUEST['action'])){
    if (function_exists($_REQUEST['action'] . "Action")){
        ($_REQUEST['action'] . "Action")();
    }else{
        echo "Function does not exist";
    }
}else{
    getupdatequestionAction();
}

if (!empty($_REQUEST["action"])) {
    ($_REQUEST["action"]."Action")();
    header("Location: http://localhost/SimpleQuizBuilder/admin/edit_question.php");
}

echo renderTpl( "html_page.phtml", ["page"=>$page]);
