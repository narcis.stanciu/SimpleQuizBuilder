<?php
/**
 * @param  string $template   
 * @param  array  $variables 
 * @return string
 */
function render( $template , $variables=[] ) {
	ob_start();
    extract($variables);
	eval(" ?>".$template."<?php ");
	$contents = ob_get_contents();
	ob_end_clean();
	return $contents;
}


/**
 * Render tpl file, supply only relative path from _TPL_PATH_
 * @param $tpl
 * @param array $variables
 * @return string
 */
function renderTpl( $tpl, $variables=[]  ){
    return render( file_get_contents(_TPL_PATH_.$tpl ),$variables );
}