<?php

function editQuiz( $dbConn, $quizname, $id){
    $result = [];
    $query = 'UPDATE quiz SET quiz_name="'.$quizname.'" WHERE quiz_id='.$id;
    $data = mysqli_query($dbConn,$query);

    if( !$data ){
        echo $query;
    }
    return $result;
}

function editQuestion( $dbConn, $name, $text, $answer, $points, $id ){
    $result = [];
    $query  = 'update questions set quest_name="'.$name.'" where question_id='.$id;
    $query2 = 'update quiz_data set question="'.$text.'",answer="'.$answer.'",points="'.$points.'" where question_id='.$id;
    $data = mysqli_query($dbConn,$query);
    $data2 = mysqli_query($dbConn,$query2);

    if( !$data and !$data2){
        echo $query;
        echo $query2;
    }
    return $result;
}

//rename ths function to addQuestion do not pollute function names with sql syntax
function insertQuiz( $dbConn, $quizname ){
    $result = [];
    $query = '
        insert into quiz (quiz_name) 
        values("'.$quizname.'")
              ';
    $data = mysqli_query($dbConn,$query);

    if( !$data ){
        echo $query;
    }
    return $result;
}

//rename ths function to addQuestion do not pollute function names with sql syntax
function insertQuestion( $dbConn, $quizid, $questionname, $questiontext, $questionanswer, $questionpoints){
    $result = [];
    $query = 'INSERT INTO questions (quest_name)
              VALUES ("'.$questionname.'")
    ';
    $query2 =  '
            INSERT INTO quiz_data (question_id, quiz_id, question, answer, points)
            SELECT q.question_id,'.$quizid.',"'.$questiontext.'","'.$questionanswer.'",'.$questionpoints.'
            FROM questions q
            ORDER BY q.question_id DESC
            LIMIT 1';
    $data = mysqli_query($dbConn,$query);
    $data2 = mysqli_query($dbConn,$query2);

    if( !$data and !$data2 ){
        echo $query;
        echo $query2;
    }
    return $result;
}

function getQuizQuestions ($dbConn, $id) {
    $result=[];
    $query = '
              SELECT * 
              FROM quiz_data 
              WHERE quiz_id ='.$id.' 
    ';
    $data = mysqli_query($dbConn,$query) or die("Bad Query");
    // NU TI_AM RECOMANDAT SA FOLOSESTI DIE ci exit
    // NU ACCEPT "OR" "AND" sau alte porcarii prin cod
    // Dupa extragerea datelor se genereaza un array cu datele preluate din bd

    if ( !$data ) {
        echo $query;
    }else{
        while( $tmp = mysqli_fetch_assoc($data) ){
            $result[] = $tmp;
        }
    }
    return $result;
}

function getQuizes ($dbConn) {
    $result=[];
    $query = 'SELECT * FROM quiz ';
    $data = mysqli_query($dbConn, $query);

    if (!$data) {
        echo $query;
    }else{
        while ($tmp = mysqli_fetch_assoc($data)){
            $result[]= $tmp;
        }
    }
    return $result;
}

//rename this function to getQuestions or getQuestionList
function getEditQuestions($dbConn) {
    $result=[];
    $query = 'select q.question_id, q.quest_name, qd.question, qd.answer, qd.points 
              from questions q 
                join quiz_data qd 
                  on q.question_id=qd.question_id';
    $data = mysqli_query($dbConn, $query);

    if (!$data) {
        echo $query;
    }else{
        while ($tmp = mysqli_fetch_assoc($data)){
            $result [] = $tmp;
        }
    }
    return $result;
}

//rename this function to getQuestionById or getQuestion
function getUpdateQuestion ($dbConn, $id) {
    $result = [];
    $query = "select * from questions where question_id=".$id." ";
    $data = mysqli_query($dbConn, $query);

    if (!$data) {
        echo $query;
    }else{
        while ($tmp = mysqli_fetch_assoc($data)){
            $result[] = $tmp;
        }
    }
    return $result;
}

//rename function to getQuizById or getQuiz
function getEditQuiz($dbConn, $id) {
    $result = [];
    $query = "select * from quiz where quiz_id=".$id." ";
    $data = mysqli_query($dbConn, $query);

    if (!$data) {
        echo $query;
    }else{
        while( $tmp = mysqli_fetch_assoc($data)){
            $result = $tmp;
        }
    }
    return $result;
}

//function getQuizMaxPoints($dbConn, $id){
//    $result = [];
//    $query = 'SELECT sum(points) as points FROM quiz_data where quiz_id ='.$id;
//    $data = mysqli_query($dbConn, $query);
//
//    if(!$data){
//        echo $query;
//    }else{
//        while ($tmp = mysqli_fetch_assoc($data)){
//            $result = $tmp;
//        }
//    }
//    return $result;
//}

//function getPoints($dbConn, $id){
//    $result = [];
//    $query = 'SELECT answer,points FROM quiz_data where quiz_id ='.$id;
//    $data = mysqli_query($dbConn, $query);
//
//    if(!$data){
//        echo $query;
//    }else{
//        while ($tmp = mysqli_fetch_assoc($data)){
//            $result = $tmp;
//        }
//    }
//    return $result;
//}
