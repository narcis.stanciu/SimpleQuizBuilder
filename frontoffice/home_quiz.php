<?php

require_once(__DIR__ . "/bootstrap.php");
require_once dirname(__DIR__)."/models/quiz.php";

global $page;
$page = [
    "id"   => "home_quiz_page"
    ,"name" => "Quizes"
    ,"tpl"  => "pages/home_quiz.phtml"
    ,"data" => []
];

function getquizesAction () {
    global $dbConn;
    global $page;
    $quizes = getQuizes($dbConn);

    if (!empty($quizes)) {
        $page['data']['quizes'] = $quizes;
    }else{
        echo "ERROR: No Quizes Data!";
    }
}

if (!empty($_REQUEST['action'])) {
    if (function_exists($_REQUEST['action'] . "Action")) {
        ($_REQUEST['action']."Action")();
    }else {
        echo "Function does not exist!";
    }
}else{   getquizesAction();
}

echo renderTpl( "html_page.phtml", ["page"=>$page]);