<?php
require_once(__DIR__ . "/bootstrap.php");
require_once dirname(__DIR__)."/models/quiz.php";

global $page;
$page = [
    "id"   => "quiz_examination_page"
    ,"name" => "Examination"
    ,"tpl"  => "pages/quiz_examination.phtml"
    ,"data" => []
];

function getquestionsAction() {
    global $dbConn;
    global $page;

    $id = $_REQUEST['id'];
    $questions = getQuizQuestions($dbConn, $id);

    if ( !empty($questions)) {
        $page["data"]["id"]        = $id;
        $page["data"]["questions"] = $questions;
    } else {
        header("Location: http://localhost/SimpleQuizBuilder/frontoffice/home_quiz.php");
    }
}

function gradeAction() {
    global $dbConn;
    global $page;
    $maxscore = 0;
    $score = 0;

    $responses = $_REQUEST['responses'];
    $questions = getQuizQuestions($dbConn, $_REQUEST['id']);

    foreach ($questions as $question) {
        $maxscore += $question['points'];
        if (!empty($question['question_id'])){
            if( $question['answer'] == $responses[$question['question_id']]){
                $score += $question['points'];
            }
        }
    }
    $page['data']['maxScore'] = $maxscore;
    $page['data']['score'] = $score;
    $page["tpl"] = 'pages/quiz_grade.phtml';

}

if (!empty($_REQUEST['action'])) {
    if( function_exists( $_REQUEST['action'] . "Action" ) ) {
        ($_REQUEST['action'] . "Action") ();
    }else{
        header("Location: http://localhost/SimpleQuizBuilder/frontoffice/home_quiz.php");
    }
} else {
    getquestionsAction();
}

echo renderTpl( "html_page.phtml", ["page"=>$page]);